import matplotlib.pyplot as plt
import numpy as np

def integrate(data, deltat):
    integ = []
    val = 0
    integ.append(val)
    for i in range(0, len(data) - 1):
        val += (data[i] + data[i + 1])*deltat/2
        integ.append(val)
    return integ

def plot(keys, data):
    for key in keys:
        plt.plot(data['T'], data[key], label=key)
    plt.legend()
    plt.show()

def main():
    # keys = ['W', 'Q1', 'Q2', 'Q3', 'AX', 'AY', 'AZ']
    keys = ['AX', 'AY', 'AZ']
    deltat = 0.00961538461538
    data = {}
    data['T'] = []
    for key in keys:
        data[key] = []

    with open("log.txt", "r") as f:
        time = 0
        for line in f:
            data['T'].append(time)
            time += deltat
            line = line.split(';')
            ind = 0
            for key in keys:
                data[key].append(float(line[ind]))
                ind += 1
    # plot(['W', 'Q1', 'Q2', 'Q3'], data)
    plot(['AX', 'AY', 'AZ'], data)
    integX = integrate(data['AX'], deltat)
    integY = integrate(data['AY'], deltat)
    integZ = integrate(data['AZ'], deltat)
    # Integrate second time
    integX = integrate(integX, deltat)
    integY = integrate(integY, deltat)
    integZ = integrate(integZ, deltat)
    plt.plot(data['T'], integX, label='X')
    plt.plot(data['T'], integY, label='Y')
    plt.plot(data['T'], integZ, label='Z')
    plt.legend()
    plt.show()

def draw_live():
    plt.axis([0, 10, 0, 1])
    for i in range(10):
        y = np.random.random()
        plt.scatter(i, y)
        plt.pause(0.05)

    plt.show()

def parse():
    keys = ['AX', 'AY', 'AZ', 'WX', 'WY', 'WZ']
    data = {}
    for key in keys:
        data[key] = []
    with open("log.txt", "r") as fin:
        for line in fin:
            line = line.split(';')
            ind = 0
            for key in keys:
                data[key].append(float(line[ind]))
                ind += 1
    with open("acc.mat", "w") as fout:
        for i in range(0, len(data[keys[0]])):
            fout.write(f"{data['AX'][i]} {data['AY'][i]} {data['AZ'][i]}\r\n")

    with open("gyro.mat", "w") as fout:
        for i in range(0, len(data[keys[0]])):
            fout.write(f"{data['WX'][i]} {data['WY'][i]} {data['WZ'][i]}\r\n")

    with open("time.mat", "w") as fout:
        deltat = 0.00961538461538
        time = 0
        for i in range(0, len(data[keys[0]])):
            fout.write(f"{time}\r\n")
            time += deltat

# main()
parse()