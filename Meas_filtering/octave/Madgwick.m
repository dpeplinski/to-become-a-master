clear;
load acc.mat
load gyro.mat
load time.mat

line_width = 1.5
figure(1);
subplot (2, 1, 1);
acc_sens = 0.000122*9.81;
plot(time, acc(:,1)*acc_sens, 'b', 'LineWidth', line_width, time, acc(:,2)*acc_sens, 'g', 'LineWidth', 1.5, time, acc(:,3)*acc_sens, 'r', 'LineWidth', 1.5);
title('Akcelerometr');
xlabel('Czas (s)');
ylabel('Przyspieszenie (m/s^2)');
legend('AccX', 'AccY', 'AccZ', "location", "southeast");

subplot (2, 1, 2)
gyro_sens = 180/pi;
plot(time, gyro(:,1)*gyro_sens, 'b', 'LineWidth', 1.5, time, gyro(:,2)*gyro_sens, 'g', 'LineWidth', 1.5, time, gyro(:,3)*gyro_sens, 'r', 'LineWidth', 1.5)
title('Żyroskop')
xlabel('Czas (s)')
ylabel('Prędkość kątowa (st./s)')
legend('GyroX', 'GyroY', 'GyroZ', "location", "southeast")

GYRO_ERROR = 3.14159265358979*(1.0/180.0);		% gyroscope measurement error in rad/s (shown as 1 deg/s)
BETA = sqrt(3.0/4.0)*GYRO_ERROR;		          % compute beta
DELTA_T = 1.0/104.0;

% Init quaternion
roll = atan2(acc(1,2), acc(1,3));
pitch = atan2(-acc(1,1), sqrt(acc(1,2)*acc(1,2) + acc(1,3)*acc(1,3)));
yaw = 0.0;

cy = cos(yaw * 0.5);
sy = sin(yaw * 0.5);
cr = cos(roll * 0.5);
sr = sin(roll * 0.5);
cp = cos(pitch * 0.5);
sp = sin(pitch * 0.5);

W = cy * cr * cp + sy * sr * sp;
Q1 = cy * sr * cp - sy * cr * sp;
Q2 = cy * cr * sp + sy * sr * cp;
Q3 = sy * cr * cp - cy * sr * sp;

QUATS = [];
ROLLS = [];
ACC_ROLLS = [];

for i=1:length(acc)
  % gyroscope and accelerometer values
  w_x_t = gyro(i,1);
  w_y_t = gyro(i,2);
  w_z_t = gyro(i,3);
  a_x_t = acc(i,1);
  a_y_t = acc(i,2);
  a_z_t = acc(i,3);
	% Axulirary variables to avoid repeated calcualtions */
	halfSEq_1 = 0.5 * W;
	halfSEq_2 = 0.5 * Q1;
	halfSEq_3 = 0.5 * Q2;
	halfSEq_4 = 0.5 * Q3;
	twoSEq_1 = 2.0 * W;
	twoSEq_2 = 2.0 * Q1;
	twoSEq_3 = 2.0 * Q2;

	% Normalise the accelerometer measurement */
	norm = sqrt(a_x_t * a_x_t + a_y_t * a_y_t + a_z_t * a_z_t);
	a_x_t /= norm;
	a_y_t /= norm;
	a_z_t /= norm;

	% Compute the objective function and Jacobian */
	f_1 = twoSEq_2 * Q3 - twoSEq_1 * Q2 - a_x_t;
	f_2 = twoSEq_1 * Q1 + twoSEq_3 * Q3 - a_y_t;
	f_3 = 1.0 - twoSEq_2 * Q1 - twoSEq_3 * Q2 - a_z_t;
	J_11or24 = twoSEq_3; 													% J_11 negated in matrix multiplication
	J_12or23 = 2.0 * Q3;
	J_13or22 = twoSEq_1; 													% J_12 negated in matrix multiplication
	J_14or21 = twoSEq_2;
	J_32 = 2.0 * J_14or21; 												% negated in matrix multiplication
	J_33 = 2.0 * J_11or24; 												% negated in matrix multiplication

	% Compute the gradient (matrix multiplication) */
	SEqHatDot_1 = J_14or21 * f_2 - J_11or24 * f_1;
	SEqHatDot_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3;
	SEqHatDot_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1;
	SEqHatDot_4 = J_14or21 * f_1 + J_11or24 * f_2;

	% Normalise the gradient */
	norm = sqrt(SEqHatDot_1 * SEqHatDot_1 + SEqHatDot_2 * SEqHatDot_2 + SEqHatDot_3 * SEqHatDot_3 + SEqHatDot_4 * SEqHatDot_4);
  if norm != 0;
    SEqHatDot_1 /= norm;
	  SEqHatDot_2 /= norm;
	  SEqHatDot_3 /= norm;
	  SEqHatDot_4 /= norm;
  endif

	% Compute the quaternion derrivative measured by gyroscopes */
	SEqDot_omega_1 = -halfSEq_2 * w_x_t - halfSEq_3 * w_y_t - halfSEq_4 * w_z_t;
	SEqDot_omega_2 = halfSEq_1 * w_x_t + halfSEq_3 * w_z_t - halfSEq_4 * w_y_t;
	SEqDot_omega_3 = halfSEq_1 * w_y_t - halfSEq_2 * w_z_t + halfSEq_4 * w_x_t;
	SEqDot_omega_4 = halfSEq_1 * w_z_t + halfSEq_2 * w_y_t - halfSEq_3 * w_x_t;

	% Compute then integrate the estimated quaternion derrivative */
	W += (SEqDot_omega_1 - (BETA * SEqHatDot_1)) * DELTA_T;
	Q1 += (SEqDot_omega_2 - (BETA * SEqHatDot_2)) * DELTA_T;
	Q2 += (SEqDot_omega_3 - (BETA * SEqHatDot_3)) * DELTA_T;
	Q3 += (SEqDot_omega_4 - (BETA * SEqHatDot_4)) * DELTA_T;

	% Normalise quaternion */
	norm = sqrt(W * W + Q1 * Q1 + Q2 * Q2 + Q3 * Q3);
	W /= norm;
	Q1 /= norm;
	Q2 /= norm;
	Q3 /= norm;
  
  % Add calculated values to the array
  QUATS = [QUATS; W Q1 Q2 Q3];
  ROLLS = [ROLLS; atan2(2.0*(W*Q1 + Q2*Q3), 1 - 2.0*(Q1*Q1 + Q2*Q2))*180.0/pi];
  ACC_ROLLS = [ACC_ROLLS; atan2(a_y_t, a_z_t)*180/pi];  
endfor

figure(2)
plot(time, QUATS(:,1), 'b', time, QUATS(:,2), 'g', time, QUATS(:,3), 'r', time, QUATS(:,4), 'k', 'LineWidth', 1.5)
title('Quaternion')
xlabel('Time')
ylabel('Estimated quaternion')
legend('W', 'Q1', 'Q2', 'Q3')

figure(3)
plot(time, ACC_ROLLS, 'r', 'LineWidth', 1.5, time, ROLLS, 'b', 'LineWidth', 1.5)
title('Madgwick')
xlabel('Czas (s)')
ylabel('Roll (stopnie)')
legend('Wartość mierzona', 'Wartość estymowana')