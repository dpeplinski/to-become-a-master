clear;
%Wczytywanie danych z pliku
load acc.mat
load gyro.mat
load time.mat

gyro_data = gyro*180/pi;
acc_data = acc;
 
%Wektor czasu
t = time;
dt = 1.0/104.0;
 
%Model w przestrzeni stanu
A = [1 -dt; 0 1];
B = [dt; 0];
C = [1 0];
 
%Szum pomiarowy i procesowy
std_dev_v = 1;
std_dev_w = 1;
V = [std_dev_v*std_dev_v*dt 0; 0 std_dev_v*std_dev_v*dt];
W = std_dev_w*std_dev_w;
 
% Wartosci poczatkowe
x0 = [0; 0];
P0 = [1 0; 0 1];
xpri = x0;
Ppri = P0;
xpost = x0;
Ppost = P0;
 
% Wektory wynikow
Y = zeros(1, size(t,2));
Yf = Y;
 
for i = 1:size(gyro_data, 1);
    %Obliczenie aktualnego kata na podstawie danych z akcelerometru
    acc_angle = atan2(acc_data(i,2), acc_data(i,3))*180/pi;
    u = gyro_data(i,1);
    Y(i) = acc_angle;
    
    if i == 1;
        % Pierwszy pomiar sluzy nam jako wartosc poczatkowa dla filtru
        xpost = [acc_angle; 0];
    else
        % aktualizacja czasu
        xpri = A*xpost + B*u;
        Ppri = A*Ppost*A' + V;
        
        % aktualizacja pomiarow
        eps = Y(i) - C*xpri;
        S = C*Ppri*C' + W;
        K = Ppri*C'*S^(-1);
        xpost = xpri + K*eps;
        Ppost = Ppri - K*S*K';
    end
    
    %Zapis estymaty do wektora wynikow
 Yf(i) = xpost(1);
end
 
plot(t, Y, 'r', 'LineWidth', 1.5, t, Yf, 'b', 'LineWidth', 1.5)
title('Kalman')
xlabel('Czas (s)')
ylabel('Roll (stopnie)')
legend('Wartość mierzona', 'Wartość estymowana')