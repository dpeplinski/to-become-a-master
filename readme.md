# Controller of a virtual robot

This repo contains my Master's thesis working progress. The main goals of a project:
- design and build the robot's hand controller
- visualize controller's movements in a virtual application
- let the controller manipulate with other objects in the application

Video below presents the current state of works:

[![](https://i.ytimg.com/vi/7wrplJhWj4U/hqdefault.jpg)](https://www.youtube.com/watch?v=7wrplJhWj4U "Video presentation")
## Implementation and technicals
The microcontroller used for quaternions computing is a Nordic Semiconductor's nRF52832 on the E73-2g4m module. The board used for data transfer between the main device and a PC is the nRF52 DK. The devices establish connection with BLE protocol. There are 11 IMUs on the main board and every IMU collects accelerometer and gyroscope data into its internal FIFO with 104Hz output data rate. The processor reads all the data, computes the quaternions and send them to the PC application approximately 60 times per second (T = 16ms). Computing the rotations and data sending takes about 2ms, so the unit still have some computing power reserve. The GUI is implemented in C++ language with OpenGL and SDL libraries.