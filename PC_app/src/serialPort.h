/*
 * serialPort.h
 *
 *  Created on: 10 Feb 2019
 *      Author: davca
 */

#ifndef SERIALPORT_H_
#define SERIALPORT_H_

#include <iostream>
#include <unistd.h>

class SerialPort {
public:
	SerialPort(std::string filename);
	virtual ~SerialPort();
	inline int IsConnected() { return !(this->serialPort < 0); }
	inline void Close() { close(this->serialPort); }
	void Write(std::string text);
	int Read(char* buf, int len);

private:
	int serialPort;
};

#endif /* SERIALPORT_H_ */
