/*
 * palmController.h
 *
 *  Created on: 24 May 2019
 *      Author: davca
 */

#ifndef PALMCONTROLLER_H_
#define PALMCONTROLLER_H_

#include <memory>
#include <boost/thread.hpp>
#include "drawing/display.h"
#include "drawing/text.h"
#include "drawing/skybox.h"
#include "timer.h"
#include "serialPort.h"
#include "humanPalm.h"

class PalmController {
public:
	PalmController(int windowHeight, int windowWidth, int groundSize, bool ifUsbUsed, bool ifUseDevPos, std::string usbPort);
	virtual ~PalmController();
	void Loop();

private:
	std::unique_ptr<Mesh> CreateGroundMesh();
	std::unique_ptr<SerialPort> InitSerialPort(std::string usbPort);
	std::unique_ptr<HumanPalm> InitPalmInstance();
	std::unique_ptr<Display> display;
	std::unique_ptr<Skybox> skybox;
	std::unique_ptr<Camera> mainCamera;
	std::unique_ptr<Camera> hudCamera;
	std::unique_ptr<Timer> timer;
	std::unique_ptr<Text> txt;
	std::unique_ptr<Shader> textureShader;
	std::unique_ptr<Shader> lightingShader;
	std::unique_ptr<Texture> bricksTexture;
	std::unique_ptr<Texture> skinTexture;
	std::unique_ptr<Mesh> groundMesh;
	std::unique_ptr<HumanPalm> rightPalm;
	struct {
		std::unique_ptr<SerialPort> portHandle;
		std::unique_ptr<boost::thread> threadHandle;
		boost::mutex dataMutex;
		float palmBatteryVoltage;
		float palmCpuUsage;
		bool isUsbUsed;
		bool ifUseDevPos;
	} usbHandler;
	int windowHeight, windowWidth, groundSize;
	void UsbHandleLoop();
	void UsbHandleUpdate(float &battVoltage, float &cpuUsage);
	void DrawEnvironment();
	void DrawPalm();
	void DrawHUD(float avgFps, float palmBattVoltage, float avgPalmCpuUsage);
	void UpdateHUD(double dt, float palmBattVoltage, float palmCpuUsage);
	void Update(double dt);
};

#endif /* PALMCONTROLLER_H_ */
