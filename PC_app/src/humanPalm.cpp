/*
 * humanPalm.cpp
 *
 *  Created on: 9 Feb 2019
 *      Author: davca
 */

#include "humanPalm.h"

HumanPalm::HumanPalm(glm::vec3 pos) {
	std::string basePath = "./res/models/FingersV2/Finger";
	float offsetsTable[] = { 37.601f, 41.273f, 47.854f, 44.257f, 36.631f };
	for (int i = 0; i < this->fingersCount; i++) {
		glm::vec3 size(offsetsTable[i], 1.0f, 1.0f);
		std::string path1 = basePath + std::to_string(i*2 + 1) + ".obj";
		std::string path2 = basePath + std::to_string(i*2 + 2) + ".obj";
		this->fingers[i] = new Finger(false, size, size, path1, path2);
	}
	this->palm = new Mesh("./res/models/FingersV2/Palm.obj");
	this->SetPos(pos);
}

HumanPalm::~HumanPalm() {
	for(int i = 0; i < this->fingersCount; i++)
		delete this->fingers[i];
}

void HumanPalm::SetPos(glm::vec3 pos) {
	this->palm->SetPos(pos);
}

void HumanPalm::SetPalmRot(glm::vec4 rot) {
	this->palm->SetRot(rot);
}

void HumanPalm::Draw(Shader &shader, Camera &camera, Texture &texture) {
	this->Update();
	texture.Bind(0);
	shader.Bind();
	for(int i = 0; i < this->fingersCount; i++)
		this->fingers[i]->Draw(shader, camera);
	this->palm->Draw(shader, camera);
}

void HumanPalm::SetFingerActive(int id, bool active) {
	if(id < this->fingersCount) {
		this->fingers[id]->SetActive(active);
	}
}

void HumanPalm::SetFingerRot(int id, glm::vec4 rot1, glm::vec4 rot2) {
	if(id < this->fingersCount) {
		this->fingers[id]->SetRot(rot1, rot2);
	}
}

void HumanPalm::Update() {
	static const glm::vec3 posOffsets[] = {
			glm::vec3(15.0f, -19.0f, -40.0f),
			glm::vec3(56.683f, -0.289f, -25.518f),
			glm::vec3(55.676f, -0.031f, -3.506f),
			glm::vec3(48.741f, -0.592f, 17.851f),
			glm::vec3(37.867f, -3.319f, 39.929f)
	};
	glm::vec3 palmPosition = this->palm->GetPos();
	glm::vec4 palmOrientation = this->palm->GetRot();
	for(int i = 0; i < this->fingersCount; i++) {
		/* Setting 'not rotated' offsets */
		glm::vec3 pos1 = posOffsets[i];
		/* Creating translation and rotation matrix */
		glm::mat4 pos1Matrix = glm::translate(pos1);
		glm::mat4 rotMatrix = glm::toMat4(glm::quat(palmOrientation.w, palmOrientation.x, palmOrientation.y, palmOrientation.z));
		/* Moving the mesh first, then rotation */
		glm::mat4 fin1Matrix = rotMatrix * pos1Matrix;
		/* Decomposing the position vector from a final matrix */
		pos1 = glm::vec3(fin1Matrix[3]);
		/* When the finger's rotation is not updated externally, set its orientation
		 * to the palm's orientation for better visual effect */
		if(this->fingers[i]->IsActive() == false) {
			this->fingers[i]->SetRot(palmOrientation, palmOrientation);
		}
		/* Moving finger's elements based on its heading */
		glm::vec4 orientation1 = this->fingers[i]->GetRot1();
		glm::vec3 heading1 = glm::rotate(glm::quat(orientation1.w, orientation1.x, orientation1.y, orientation1.z), glm::vec3(1.0f, 0.0f, 0.0f));
		glm::vec3 pos2 = pos1 + heading1*(this->fingers[i]->GetSize1().x);
		this->fingers[i]->SetPos(pos1 + palmPosition, pos2 + palmPosition);
	}
}
