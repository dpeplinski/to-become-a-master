#ifndef CUBOID_H_INCLUDED
#define CUBOID_H_INCLUDED

#include "../drawing/mesh.h"

class Cuboid {
public:
	Cuboid(glm::vec3 size) {
		this->size = size;
		Vertex vertices[] = {
			Vertex(glm::vec3(-size.x/2.0, -size.y/2.0, size.z/2.0), glm::vec2(0.0, 1.0), glm::vec3(-1.0, -1.0, 1.0)),
			Vertex(glm::vec3(size.x/2.0, -size.y/2.0, size.z/2.0), glm::vec2(1.0, 1.0), glm::vec3(1.0,-1.0, 1.0)),
			Vertex(glm::vec3(size.x/2.0, size.y/2.0, size.z/2.0), glm::vec2(1.0, 0.0), glm::vec3(1.0, 1.0, 1.0)),
			Vertex(glm::vec3(-size.x/2.0, size.y/2.0, size.z/2.0), glm::vec2(0.0, 0.0), glm::vec3(-1.0, 1.0, 1.0)),

			Vertex(glm::vec3(-size.x/2.0, -size.y/2.0, -size.z/2.0), glm::vec2(1.0, 1.0), glm::vec3(-1.0, -1.0, -1.0)),
			Vertex(glm::vec3(size.x/2.0, -size.y/2.0, -size.z/2.0), glm::vec2(0.0, 1.0), glm::vec3(1.0, -1.0, -1.0)),
			Vertex(glm::vec3(size.x/2.0, size.y/2.0, -size.z/2.0), glm::vec2(0.0, 0.0), glm::vec3(1.0, 1.0, -1.0)),
			Vertex(glm::vec3(-size.x/2.0, size.y/2.0, -size.z/2.0), glm::vec2(1.0, 0.0), glm::vec3(-1.0, 1.0, -1.0)),

			Vertex(glm::vec3(size.x/2.0, size.y/2.0, -size.z/2.0), glm::vec2(1.0, 1.0), glm::vec3(1.0, 1.0, -1.0)),
			Vertex(glm::vec3(-size.x/2.0, size.y/2.0, -size.z/2.0), glm::vec2(0.0, 1.0), glm::vec3(-1.0, 1.0, -1.0)),

			Vertex(glm::vec3(-size.x/2.0, -size.y/2.0, -size.z/2.0), glm::vec2(0.0, 0.0), glm::vec3(-1.0, -1.0, -1.0)),
			Vertex(glm::vec3(size.x/2.0, -size.y/2.0, -size.z/2.0), glm::vec2(1.0, 0.0), glm::vec3(1.0, -1.0, -1.0)),
		};
		unsigned int indices[] = {
			4, 0, 3, 4, 3, 7, 1, 5, 6, 1, 6, 2,	// X axis
			0, 1, 2, 0, 2, 3, 5, 4, 7, 5, 7, 6,	// Z axis
			3, 2, 8, 3, 8, 9,					// top wall
			0, 10, 11, 0, 11, 1,				// bottom wall
		};
		this->mesh = new Mesh(vertices, sizeof(vertices)/sizeof(vertices[0]), indices, sizeof(indices)/sizeof(indices[0]));
	}

	~Cuboid() {
		delete this->mesh;
	}

	void Draw(Shader& shader, Camera& camera) {
		this->mesh->Draw(shader, camera);
	}

	void SetPos(glm::vec3 pos) {
		this->mesh->SetPos(pos);
	}

	void SetRot(glm::vec4 rot) {
		this->mesh->SetRot(rot);
	}

	glm::vec3 GetPos() {
		return this->mesh->GetPos();
	}

	glm::vec4 GetRot() {
		return this->mesh->GetRot();
	}

	glm::vec3 GetSize() {
		return this->size;
	}

protected:
private:
	Mesh* mesh;
	glm::vec3 size;
};

#endif // CUBOID_H_INCLUDED
