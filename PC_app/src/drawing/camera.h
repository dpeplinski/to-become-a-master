#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <iostream>
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

class Camera {
public:
	Camera(const glm::vec3& pos, const glm::vec3& lookAt, float fov, float aspect, float zNear, float zFar) {
		/* We do not create setters and getters, because the perspective changes only when windows resizes */
		/* We can add setters/getters for camera angles if we want to change them in future */
		m_perspective = glm::perspective((float)(fov*M_PI/180.0), aspect, zNear, zFar);
		m_position = pos;
		/* Looking into the center of sreen (opposite to camera's pos) */
		m_forward = lookAt;
		/* The Y-axis as up angle */
		m_up = glm::vec3(0,1,0);
	}

	inline glm::mat4 GetViewProjection() const {
		return m_perspective * glm::lookAt(m_position, m_position + m_forward, m_up);
	}

	inline glm::vec3 GetPos() {
		return m_position;
	}

	inline glm::vec3 GetLookAt() {
		return m_forward;
	}

	inline void Move(glm::vec3 pos) {
		m_position += pos;
	}

	inline void MoveForward(float v) {
		m_position += m_forward*v;
	}

	inline void MoveBackward(float v) {
		m_position -= m_forward*v;
	}

	inline void MoveRight(float v) {
		m_position += glm::normalize(glm::vec3(-m_forward.z, 0.0f, m_forward.x))*v;
	}

	inline void MoveLeft(float v) {
		m_position -= glm::normalize(glm::vec3(-m_forward.z, 0.0f, m_forward.x))*v;
	}

	inline void MoveDownward(float v) {
		m_position -= glm::vec3(0.0f, v, 0.0f);
	}

	inline void Rotate(float x_angle, float y_angle) {
		glm::vec3 v = m_forward;
		/* Rotate around y axis (0*i + 1*j + 0*k), where:
		 * q = w + i*x + j*y + k*z;
		 * q = cos(angle/2) + sin(angle/2)*(x*i + y*j + z*k);
		 * and (x*i + y*j + z*k) must be a unit vector */
		glm::quat q(cosf(x_angle*M_PI/180.0f/2.0f), 0.0f, sinf(x_angle*M_PI/180.0f/2.0f), 0.0f);
		v = glm::rotate(q, v);
		/* Rotate around the axis that is perpendicular to a current xz vector */
		if((v.y < 0.975 && y_angle > 0) || (v.y > -0.975 && y_angle < 0)) {
			/* Vector that is perpendicular to (x, 0, z) */
			glm::vec3 perp = glm::normalize(glm::vec3(-v.z, 0.0f, v.x));
			q = glm::quat(cosf(y_angle*M_PI/180.0f/2.0f), sinf(y_angle*M_PI/180.0f/2.0f)*perp.x, perp.y, sinf(y_angle*M_PI/180.0f/2.0f)*perp.z);
			v = glm::rotate(q, v);
		}
		/* Normalising vector after every multiplication */
		m_forward = glm::normalize(v);
	}

protected:
private:
	glm::mat4 m_perspective;
	glm::vec3 m_position;
	glm::vec3 m_forward;
	glm::vec3 m_up;
};

#endif	// CAMERA_H_INCLUDED
