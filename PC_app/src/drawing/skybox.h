/*
 * skybox.h
 *
 *  Created on: 23 May 2019
 *      Author: davca
 */

#ifndef ENGINE_WORLD_SKYBOX_H_
#define ENGINE_WORLD_SKYBOX_H_

#include "../drawing/mesh.h"

class Skybox {
public:
	Skybox(int size, std::string texture) {
		m_size = size;
		m_texture = new Texture(texture, false);
		Vertex vertices[] = {
			// left
			Vertex(glm::vec3(-size, -size, size), glm::vec2(0.0f, 0.6667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, -size, -size), glm::vec2(0.25f, 0.6667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, size, -size), glm::vec2(0.25f, 0.3334f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, size, size), glm::vec2(0.0f, 0.3334f), glm::vec3(0, 0, 0)),
			// front
			Vertex(glm::vec3(-size, -size, -size), glm::vec2(0.25f, 0.66667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, -size, -size), glm::vec2(0.5f, 0.66667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, size, -size), glm::vec2(0.5f, 0.33334f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, size, -size), glm::vec2(0.25f, 0.33334f), glm::vec3(0, 0, 0)),
			// right
			Vertex(glm::vec3(size, -size, -size), glm::vec2(0.5f, 0.66667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, -size, size), glm::vec2(0.75f, 0.66667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, size, size), glm::vec2(0.75f, 0.33334f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, size, -size), glm::vec2(0.5f, 0.33334f), glm::vec3(0, 0, 0)),
			// back
			Vertex(glm::vec3(size, -size, size), glm::vec2(0.75f, 0.66667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, -size, size), glm::vec2(1.0f, 0.66667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, size, size), glm::vec2(1.0f, 0.33334f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, size, size), glm::vec2(0.75f, 0.33334f), glm::vec3(0, 0, 0)),
			// up
			Vertex(glm::vec3(-size, size, -size), glm::vec2(0.25f, 0.33334f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, size, -size), glm::vec2(0.5f, 0.33334f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, size, size), glm::vec2(0.5f, 0.0f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, size, size), glm::vec2(0.25f, 0.0f), glm::vec3(0, 0, 0)),
			// down
			Vertex(glm::vec3(-size, -size, size), glm::vec2(0.25f, 1.0f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, -size, size), glm::vec2(0.5f, 1.0f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(size, -size, -size), glm::vec2(0.5f, 0.66667f), glm::vec3(0, 0, 0)),
			Vertex(glm::vec3(-size, -size, -size), glm::vec2(0.25f, 0.66667f), glm::vec3(0, 0, 0))
		};
		unsigned int indices[] = {
			// left
			0, 1, 2, 0, 2, 3,
			// front
			4, 5, 6, 4, 6, 7,
			// right
			8, 9, 10, 8, 10, 11,
			// back
			12, 13, 14, 12, 14, 15,
			// up
			16, 17, 18, 16, 18, 19,
			// down
			20, 21, 22, 20, 22, 23
		};
		m_walls = new Mesh(vertices, sizeof(vertices)/sizeof(vertices[0]), indices, sizeof(indices)/sizeof(indices[0]));
	}

	~Skybox() {
		delete m_walls;
		delete m_texture;
	}

	void SetPos(glm::vec3 pos) {
		m_walls->SetPos(pos);
	}

	void SetRot(glm::vec4 rot) {
		m_walls->SetRot(rot);
	}

	void Draw(Shader& shader, Camera& camera) {
		m_walls->Draw(*m_texture, shader, camera);
	}

protected:
private:
	Mesh* m_walls;
	Texture* m_texture;
	int m_size;
};

#endif /* ENGINE_WORLD_SKYBOX_H_ */
