#include "palmController.h"

#define WIDTH 2560
#define HEIGHT 1440
#define USE_USB_DEV 1
#define USE_DEV_POS 1
#define GROUND_SIZE 0

int main(int argc, char **argv) {
	PalmController mainController(HEIGHT, WIDTH, GROUND_SIZE, USE_USB_DEV, USE_DEV_POS, (1 < argc) ? argv[1] : "/dev/ttyACM0");
	mainController.Loop();
	return 0;
}
