/*
 * timer.h
 *
 *  Created on: 19 Dec 2018
 *      Author: davca
 */

#ifndef ENGINE_CORE_TIMER_H_
#define ENGINE_CORE_TIMER_H_

#include <chrono>

class Timer {
public:
	Timer() {
		this->applicationStart = std::chrono::system_clock::now();
		this->lastDtTime = applicationStart;
	}
	/* This function returns the amount of milis between the last and current call of this function. */
	double GetDt() {
		auto currentDtTime = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsedTime = this->lastDtTime - currentDtTime;
		this->lastDtTime = currentDtTime;
		return (double)-elapsedTime.count()*1000.0;
	}
private:
	std::chrono::time_point<std::chrono::system_clock> applicationStart;
	std::chrono::time_point<std::chrono::system_clock> lastDtTime;
};

#endif /* ENGINE_CORE_TIMER_H_ */
