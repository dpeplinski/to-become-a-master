/*
 * serialPort.cpp
 *
 *  Created on: 10 Feb 2019
 *      Author: davca
 */

#include "serialPort.h"
#include <cstring>
#include <termios.h> // Contains POSIX terminal control definitions
#include <cerrno> // Error integer and strerror() function
#include <fcntl.h> // Contains file controls like O_RDWR

SerialPort::SerialPort(std::string filename) {
	/* Device must be connected and a user must be in correct group, for arch linux:
	 * sudo usermod -a -G uucp username
	 * groups username - shows current groups memberity */
	this->serialPort = open(filename.c_str(), O_RDWR | O_NONBLOCK);
	/* Create new termios struct, call it 'tty' for convention */
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	/* Read in existing settings, and handle any error */
	if(tcgetattr(this->serialPort, &tty) != 0) {
		goto error;
	} else {
		/* Control modes */
		tty.c_cflag &= ~PARENB; 				// Clear parity bit, disabling parity
		tty.c_cflag &= ~CSTOPB; 				// Clear stop field, only one stop bit used in communication
		tty.c_cflag |= CS8; 					// 8 bits per byte
		tty.c_cflag &= ~CRTSCTS; 				// Disable RTS/CTS hardware flow control
		tty.c_cflag |= CREAD | CLOCAL;			// Turn on READ & ignore ctrl lines (CLOCAL = 1)
		/* Local modes */
		tty.c_lflag |= ICANON;					// Canonical mode - processing data after new line character - '\n'
		tty.c_lflag &= ~ECHO; 					// Disable echo
		tty.c_lflag &= ~ECHOE; 					// Disable erasure
		tty.c_lflag &= ~ECHONL; 				// Disable new-line echo
		tty.c_lflag &= ~ISIG; 					// Disable interpretation of INTR, QUIT and SUSP
		/* Input modes */
		tty.c_iflag &= ~(IXON | IXOFF | IXANY);	// Turn off s/w flow ctrl
		tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes
		/* Output modes */
		tty.c_oflag &= ~OPOST; 					// Prevent special interpretation of output bytes (e.g. newline chars)
		tty.c_oflag &= ~ONLCR; 					// Prevent conversion of newline to carriage return/line feed
		// tty.c_oflag &= ~OXTABS; 				// Prevent conversion of tabs to spaces (NOT PRESENT IN LINUX)
		// tty.c_oflag &= ~ONOEOT; 				// Prevent removal of C-d chars (0x004) in output (NOT PRESENT IN LINUX)
		tty.c_cc[VTIME] = 0;    				// Non blocking mode - returns immadiately whatever is available
		tty.c_cc[VMIN] = 0;
		/* Set in/out baud rate to be 460800 */
		cfsetispeed(&tty, B460800);
		cfsetospeed(&tty, B460800);
		/* Save tty settings, also check for error */
		if(tcsetattr(this->serialPort, TCSANOW, &tty) != 0) {
		    goto error;
		}
		else {
			goto exit;
		}
	}
exit:
	return;
error:
	std::cerr << "Error " << errno << " from tcgetattr: " << strerror(errno) << std::endl;
	this->Close();
	return;
}

SerialPort::~SerialPort() {
	close(this->serialPort);
}

void SerialPort::Write(std::string text) {
	if(this->IsConnected())
		write(this->serialPort, text.c_str(), strlen(text.c_str()));
}

int SerialPort::Read(char* buf, int len) {
	/* Read bytes. The behaviour of read() (e.g. does it block?)
	 * how long does it block for?) depends on the configuration
	 * settings above, specifically VMIN and VTIME
	 * returns the number of bytes read. It may be 0 if no bytes were received, and can also be negative to signal an error. */
	return read(this->serialPort, buf, len);
}
