/*
 * palmController.cpp
 *
 *  Created on: 24 May 2019
 *      Author: davca
 */

#include "palmController.h"
#include "base64.h"
#include <GL/glew.h>
#include <iomanip>
#include <boost/chrono.hpp>

PalmController::PalmController(int windowHeight, int windowWidth, int groundSize, bool isUsbUsed, bool ifUseDevPos, std::string usbPort) {
	this->windowHeight = windowHeight;
	this->windowWidth = windowWidth;
	this->groundSize = groundSize;
	this->display = std::unique_ptr<Display>(new Display(windowWidth, windowHeight, "ThisIsMyMastery"));
	this->skybox = std::unique_ptr<Skybox>(new Skybox(10000, "./res/textures/skyboxes/space.png"));
	this->skybox->SetRot(glm::vec4(0.0f, 1.0f, 0.0f, 0.0f));
	this->mainCamera = std::unique_ptr<Camera>(new Camera(glm::vec3(-183.353f, 242.36f, -323.089f), glm::vec3(0.608287f, -0.374607f, 0.699755f), 70.0f, (float)windowWidth/(float)windowHeight, 0.01f, 100000.0f));
	this->hudCamera = std::unique_ptr<Camera>(new Camera(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0, 0.0, -1.0), 70.0f, (float)windowWidth/(float)windowHeight, 0.01f, 1000.0f));
	this->timer = std::unique_ptr<Timer>(new Timer());
	this->txt = std::unique_ptr<Text>(new Text());
	this->textureShader = std::unique_ptr<Shader>(new Shader("./res/shaders/texture"));
	this->lightingShader = std::unique_ptr<Shader>(new Shader("./res/shaders/light"));
	this->bricksTexture = std::unique_ptr<Texture>(new Texture("./res/textures/bricks.jpg", true));
	this->skinTexture = std::unique_ptr<Texture>(new Texture("./res/textures/steel.jpg", true));
	this->groundMesh = this->CreateGroundMesh();
	this->rightPalm = this->InitPalmInstance();
	this->usbHandler.isUsbUsed = isUsbUsed;
	this->usbHandler.ifUseDevPos = ifUseDevPos;
	this->usbHandler.portHandle = this->InitSerialPort(usbPort);
	this->usbHandler.threadHandle = std::unique_ptr<boost::thread>(new boost::thread(&PalmController::UsbHandleLoop, this));
}

PalmController::~PalmController() {
	this->usbHandler.threadHandle->join();
	if(this->usbHandler.isUsbUsed) {
		this->usbHandler.portHandle->Close();
		std::cout << "Serial port closed" << std::endl;
	}
	else {
		std::cout << "Application closed" << std::endl;
	}
}

std::unique_ptr<Mesh> PalmController::CreateGroundMesh() {
	Vertex groundVertices[] = {
			Vertex(glm::vec3(-this->groundSize/2.0f, 0.0f, this->groundSize/2.0f), glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
			Vertex(glm::vec3(this->groundSize/2.0f, 0.0f, this->groundSize/2.0f), glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
			Vertex(glm::vec3(this->groundSize/2.0f, 0.0f, -this->groundSize/2.0f), glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
			Vertex(glm::vec3(-this->groundSize/2.0f, 0.0f, -this->groundSize/2.0f), glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
	};
	unsigned int groundIndices[] = { 0, 1, 2, 0, 2, 3 };
	return std::unique_ptr<Mesh>(new Mesh(groundVertices, sizeof(groundVertices)/sizeof(groundVertices[0]), groundIndices, sizeof(groundIndices)/sizeof(groundIndices[0])));
}

std::unique_ptr<SerialPort> PalmController::InitSerialPort(std::string usbPort) {
	if(this->usbHandler.isUsbUsed) {
		std::unique_ptr<SerialPort> usb = std::unique_ptr<SerialPort>(new SerialPort(usbPort));
		if(!usb->IsConnected()) {
			std::cerr << "Failed to open serial port!" << std::endl;
			return NULL;
		} else {
			return usb;
		}
	} else {
		return NULL;
	}
}

std::unique_ptr<HumanPalm> PalmController::InitPalmInstance() {
	std::unique_ptr<HumanPalm> hand = std::unique_ptr<HumanPalm>(new HumanPalm(glm::vec3(0.0f, 75.0f, 0.0f)));
	hand->SetFingerActive(0, true);
	hand->SetFingerActive(1, true);
	hand->SetFingerActive(2, true);
	hand->SetFingerActive(3, true);
	hand->SetFingerActive(4, true);
	return hand;
}

void PalmController::UsbHandleLoop() {
	typedef struct {
		float W;
		float Q1;
		float Q2;
		float Q3;
	} quat_s;

	typedef struct {
		float x;
		float y;
		float z;
	} vec3f_s;

	typedef struct {
		vec3f_s position;
		uint16_t batt_voltage;
		uint16_t cpu_usage;
		uint8_t bytes_count;
		uint8_t imus_count;
	} misc_data_s;

	while(!this->display->isClosed()) {
		if(this->usbHandler.isUsbUsed) {
			const int USB_BUF_LEN = 1024;
			char usbBuf[USB_BUF_LEN];
			unsigned char binBuf[USB_BUF_LEN];
			int textLen = this->usbHandler.portHandle->Read(usbBuf, USB_BUF_LEN);
			if(0 < textLen) {
				int bytes_count = BASE64_Decode(binBuf, (unsigned char *)usbBuf, USB_BUF_LEN);
				unsigned char *ptr = binBuf;
				misc_data_s *misc_data_ptr = (misc_data_s *)ptr;
				ptr += sizeof(misc_data_s);
				if (misc_data_ptr->bytes_count == bytes_count) {
					this->usbHandler.dataMutex.lock();
					this->usbHandler.palmBatteryVoltage = misc_data_ptr->batt_voltage/1000.0f;
					this->usbHandler.palmCpuUsage = misc_data_ptr->cpu_usage/10.0f;
					if (this->usbHandler.ifUseDevPos)
						this->rightPalm->SetPos(glm::vec3(misc_data_ptr->position.x, misc_data_ptr->position.z, -misc_data_ptr->position.y));
					quat_s * meas_ptr = (quat_s *)ptr;
					this->rightPalm->SetPalmRot(glm::vec4(meas_ptr->Q1, meas_ptr->Q3, -meas_ptr->Q2, meas_ptr->W));
					meas_ptr++;
					for(int i = 1; i < misc_data_ptr->imus_count; i+=2) {
						glm::vec4 q1 = glm::vec4(meas_ptr->Q1, meas_ptr->Q3, -meas_ptr->Q2, meas_ptr->W);
						meas_ptr++;
						glm::vec4 q2 = glm::vec4(meas_ptr->Q1, meas_ptr->Q3, -meas_ptr->Q2, meas_ptr->W);
						meas_ptr++;
						this->rightPalm->SetFingerRot((i - 1)/2, q1, q2);
					}
					this->usbHandler.dataMutex.unlock();
				}
			}
		} else {
			this->usbHandler.dataMutex.lock();
			glm::vec4 q(0.0f, 0.0f, 0.383f, 0.924f);
			this->rightPalm->SetFingerRot(0, glm::vec4(-0.707f, 0.0f, 0.0f, 0.707f), glm::vec4(-0.542f, -0.420f, -0.335f, 0.646f));
			this->rightPalm->SetFingerRot(1, q, q);
			this->rightPalm->SetFingerRot(2, glm::vec4(0.0f, 0.0f, -0.383f, 0.924f), glm::vec4(0.0f, 0.0f, -0.924f, 0.383f));
			this->rightPalm->SetFingerRot(3, glm::vec4(0.0f, 0.0f, -0.383f, 0.924f), glm::vec4(0.0f, 0.0f, -0.924f, 0.383f));
			this->rightPalm->SetFingerRot(4, glm::vec4(0.0f, 0.0f, -0.383f, 0.924f), glm::vec4(0.0f, 0.0f, -0.924f, 0.383f));
			this->rightPalm->SetPalmRot(q);
			this->usbHandler.dataMutex.unlock();
		}
		boost::this_thread::sleep_for(boost::chrono::milliseconds{1});
	}
}

void PalmController::DrawPalm() {
	this->rightPalm->Draw(*this->lightingShader, *this->mainCamera, *this->skinTexture);
}

void PalmController::UsbHandleUpdate(float &battVoltage, float &cpuUsage) {
	this->usbHandler.dataMutex.lock();
	battVoltage = this->usbHandler.palmBatteryVoltage;
	cpuUsage = this->usbHandler.palmCpuUsage;
	this->DrawPalm();
	this->usbHandler.dataMutex.unlock();
}

void PalmController::UpdateHUD(double dt, float palmBattVoltage, float palmCpuUsage) {
	static int framesCount = 0;
	static double totalDt = 0.0;
	static float totalPalmCpuUsage = 0.0f;
	static float avgFps = 0.0f;
	static float avgPalmCpuUsage = 0.0f;
	framesCount++;
	totalDt += dt;
	totalPalmCpuUsage += palmCpuUsage;
	if (totalDt > 100.0) {
		avgFps = totalDt/framesCount;
		avgPalmCpuUsage = totalPalmCpuUsage/framesCount;
		framesCount = 0;
		totalDt = 0;
		totalPalmCpuUsage = 0;
	}
	this->DrawHUD(avgFps, palmBattVoltage, avgPalmCpuUsage);
}

void PalmController::DrawHUD(float avgFps, float palmBattVoltage, float avgPalmCpuUsage) {
	this->txt->Print("MyMaster v1.9", glm::vec3(-1.2f, 0.67f, 0.0f), 0.03f, *this->hudCamera);
	std::stringstream fps;
	fps << std::fixed << std::setprecision(1) << avgFps << " ms";
	this->txt->Print(fps.str(), glm::vec3(1.05f, 0.67f, 0.0f), 0.03f, *this->hudCamera);
	std::stringstream v;
	v << std::fixed << std::setprecision(2) << palmBattVoltage << " V";
	this->txt->Print(v.str(), glm::vec3(1.05f, 0.62f, 0.0f), 0.03f, *this->hudCamera);
	std::stringstream cpu;
	cpu << std::fixed << std::setprecision(1) << avgPalmCpuUsage << " %";
	this->txt->Print(cpu.str(), glm::vec3(1.05f, 0.57f, 0.0f), 0.03f, *this->hudCamera);
}

void PalmController::DrawEnvironment() {
	this->skybox->SetPos(this->mainCamera->GetPos());
	this->skybox->Draw(*this->textureShader, *this->mainCamera);
	this->groundMesh->Draw(*this->bricksTexture, *this->textureShader, *this->mainCamera);
}

void PalmController::Update(double dt) {
	float battVoltage, cpuUsage;
	this->DrawEnvironment();
	this->UsbHandleUpdate(battVoltage, cpuUsage);
	this->UpdateHUD(dt, battVoltage, cpuUsage);
	this->display->Update(*this->mainCamera, dt);
}

void PalmController::Loop() {
	while(!this->display->isClosed()) {
		double dt = this->timer->GetDt();

		this->Update(dt);
	}
}
