/*
 * humanPalm.h
 *
 *  Created on: 9 Feb 2019
 *      Author: davca
 */

#ifndef HUMANPALM_H_
#define HUMANPALM_H_

#include "drawing/cuboid.h"

class Finger {
public:
	Finger(bool isActive, const glm::vec3 size1, const glm::vec3 size2, std::string& finger1, std::string& finger2) {
		this->isActive = isActive;
		this->rot[0] = glm::vec4(0, 0, 0, 1);
		this->rot[1] = glm::vec4(0, 0, 0, 1);
		this->size[0] = size1;
		this->size[1] = size2;
		this->pos[0] = glm::vec3(0, 0, 0);
		this->pos[1] = glm::vec3(0, 0, 0);
		this->cuboids[0] = new Mesh(finger1);
		this->cuboids[1] = new Mesh(finger2);
		this->SetPos(this->pos[0], this->pos[1]);
		this->SetRot(this->rot[0], this->rot[0]);
	}

	~Finger() {
		for(int i = 0; i < (int)(sizeof(this->cuboids)/sizeof(this->cuboids[0])); i++)
			delete this->cuboids[i];
	}

	void SetPos(glm::vec3 pos1, glm::vec3 pos2) {
		this->pos[0] = pos1;
		this->pos[1] = pos2;
		this->cuboids[0]->SetPos(pos1);
		this->cuboids[1]->SetPos(pos2);
	}

	void SetActive(bool active) {
		this->isActive = active;
	}

	void SetRot(glm::vec4 rot1, glm::vec4 rot2) {
		this->rot[0] = rot1;
		this->rot[1] = rot2;
		for(int i = 0; i < 2; i++) {
			this->cuboids[i]->SetRot(this->rot[i]);
		}
	}

	bool IsActive() { return this->isActive; }
	glm::vec3 GetSize1() { return this->size[0]; }
	glm::vec3 GetSize2() { return this->size[1]; }
	glm::vec4 GetRot1() { return this->rot[0]; }
	glm::vec4 GetRot2() { return this->rot[1]; }

	void Draw(Shader& shader, Camera& camera) {
		for(int i = 0; i < (int)(sizeof(this->cuboids)/sizeof(this->cuboids[0])); i++)
			this->cuboids[i]->Draw(shader, camera);
	}

private:
	Mesh *cuboids[2];
	glm::vec3 pos[2];
	glm::vec4 rot[2];
	glm::vec3 size[2];
	bool isActive;
};

class HumanPalm {
public:
	HumanPalm(glm::vec3 pos);
	virtual ~HumanPalm();
	void Draw(Shader& shader, Camera& camera, Texture &texture);
	void SetPos(glm::vec3 pos);
	void SetPalmRot(glm::vec4 rot);
	void SetFingerActive(int id, bool active);
	void SetFingerRot(int id, glm::vec4 rot1, glm::vec4 rot2);

private:
	void Update();
	static const int fingersCount = 5;
	Finger *fingers[fingersCount];
	Mesh *palm;
};

#endif /* HUMANPALM_H_ */
