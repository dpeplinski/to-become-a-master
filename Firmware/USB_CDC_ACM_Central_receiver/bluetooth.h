/*
 * bluetooth.h
 *
 *  Created on: Jan 19, 2020
 *      Author: davca
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include "bsp.h"

void bluetooth_init();
void bluetooth_handle_bsp_event(bsp_event_t event);
void scan_start();

#endif /* BLUETOOTH_H_ */
