/*
 * usb.h
 *
 *  Created on: Jan 19, 2020
 *      Author: davca
 */

#ifndef USB_H_
#define USB_H_

#include "bsp.h"

void usb_init();
bool usb_is_busy();
void usb_send_data(uint8_t *data, uint32_t len);
void usb_loop();

#endif /* USB_H_ */
