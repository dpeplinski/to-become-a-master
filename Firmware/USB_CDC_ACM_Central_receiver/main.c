#include <stdint.h>
#include "app_timer.h"
#include "bsp_btn_ble.h"
#include "bluetooth.h"
#include "usb.h"

void timer_init() {
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

void bsp_event_handler(bsp_event_t event) {
	bluetooth_handle_bsp_event(event);
}

void buttons_leds_init() {
	ret_code_t err_code;
	err_code = bsp_init(BSP_INIT_LEDS, bsp_event_handler);
	APP_ERROR_CHECK(err_code);
}

int main(void) {
    // Initialize
    timer_init();
    buttons_leds_init();
    bluetooth_init();
    usb_init();
    scan_start();

    while (1) {
    	usb_loop();
    }
}
