/*
 * debug.c
 *
 *  Created on: Sep 10, 2019
 *      Author: davca
 */

#include "debug.h"
#include "nrfx_timer.h"

static const nrfx_timer_t DEBUG_TIMER = NRFX_TIMER_INSTANCE(1);
static unsigned int debug_time;

void debug_timer_handler(nrf_timer_event_t event_type, void * p_context) { }

void debug_timer_init() {
	nrfx_timer_config_t debug_timer_config = NRFX_TIMER_DEFAULT_CONFIG;
	nrfx_timer_init(&DEBUG_TIMER, &debug_timer_config, debug_timer_handler);
	nrfx_timer_enable(&DEBUG_TIMER);
}

inline void debug_timer_clear() {
	DEBUG_TIMER.p_reg->TASKS_CLEAR = 0x1UL;
}

inline void debug_timer_capture() {
	DEBUG_TIMER.p_reg->TASKS_CAPTURE[0] = 0x1UL;
	debug_time = (unsigned int)DEBUG_TIMER.p_reg->CC[0];
}

inline unsigned int debug_timer_get() {
	return debug_time;
}
