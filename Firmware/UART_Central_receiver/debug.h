/*
 * debug.h
 *
 *  Created on: Sep 10, 2019
 *      Author: davca
 */

#ifndef DEBUG_H_
#define DEBUG_H_

void debug_timer_init();
void debug_timer_clear();
void debug_timer_capture();
unsigned int debug_timer_get();

#endif /* DEBUG_H_ */
