/*
 * systemTimer.h
 *
 *  Created on: Sep 14, 2019
 *      Author: davca
 */

#ifndef SYSTEMTIMER_H_
#define SYSTEMTIMER_H_

#include <stdint.h>

void SYSTIM_Init();
uint32_t SYSTIM_GetTick();
void SYSTIM_ResetTick();

#endif /* SYSTEMTIMER_H_ */
