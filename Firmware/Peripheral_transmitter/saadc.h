/*
 * saadc.h
 *
 *  Created on: Jan 18, 2020
 *      Author: davca
 */

#ifndef SAADC_H_
#define SAADC_H_

#include <stdint.h>

void saadc_init();
void saadc_meas_batt_voltage();
uint32_t saadc_get_current_batt_voltage();

#endif /* SAADC_H_ */
