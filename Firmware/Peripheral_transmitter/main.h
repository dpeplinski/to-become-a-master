/*
 * main.h
 *
 *  Created on: 10 Feb 2019
 *      Author: davca
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "nrfx_timer.h"

#define DEBUG_MODE 			0
#define CALCULATE_POS		0
#define BUTTON_PIN 			13

typedef enum {
	LED_RED = 14,
	LED_GRN = 15,
	LED_BLUE = 16
} led_t;

typedef enum {
	LED_STATE_SET,
	LED_STATE_CLEAR,
	LED_STATE_TOGGLE
} led_state_t;

void BleprintText(const char * text);
void BleSendBytes(uint8_t *data, uint16_t *count);
void set_led_state(led_t led, led_state_t state);

#endif /* MAIN_H_ */
