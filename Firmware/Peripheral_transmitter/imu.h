#ifndef __IMU_H
#define __IMU_H

#include <stdbool.h>
#include <stdint.h>

#define REFRESH_T				12										// A period (in ms) of quaternions refreshing and sending to the central receiver
#define	IMU_ODR					0b0100
#define IMU_SAMPLE_F			104

#define IASSERT(x)				APP_ERROR_CHECK(!(x))

/* Device's global states */
typedef enum {
	STARTUP,
	CALIBRATE,
	MEASURE,
	IDLE
} dev_state_t;

typedef enum {
	CS1 = 24,
	CS2 = 23,
	CS3 = 18,
	CS4 = 17,
	CS5 = 11,
	CS6 = 10,
	CS7 = 6,
	CS8 = 5,
	CS9 = 31,
	CS10 = 30,
	CS11 = 26,
	CS12 = 25,
	CS13 = 12
} imu_cs_t;

void PORT_SetDevState(dev_state_t state);
void PORT_SpiInit();
void PORT_ImuMotionTrackingLoop(bool *if_continue);

#endif
