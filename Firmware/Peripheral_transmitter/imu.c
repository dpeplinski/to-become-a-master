#include "imu.h"
#include "saadc.h"
#include "main.h"
#include "debug.h"
#include "systemTimer.h"
#include <string.h>
#include "math.h"
#include "nrfx_spim.h"
#include "nrfx_gpiote.h"
#include "nrf_gpio.h"
#include "nrf_sdh.h"
#include "nrf_delay.h"
#include "nrf_error.h"
#include "quatMath.h"

static volatile dev_state_t dev_state;

/* LSM6DSM registers */
typedef enum {
	FUNC_CFG_ACCESS = 0x01,
	INT1_CTRL = 0x0d,
	WHO_AM_I = 0x0f,
	FIFO_CTRL1 = 0x06,
	FIFO_CTRL2 = 0x07,
	FIFO_CTRL3 = 0x08,
	FIFO_CTRL4 = 0x09,
	FIFO_CTRL5 = 0x0A,
	CTRL1_XL = 0x10,
	CTRL2_G = 0x11,
	CTRL3_C = 0x12,
	CTRL6_C = 0x15,
	CTRL8_XL = 0x17,
	CTRL10_C = 0x19,
	WAKE_UP_SRC = 0x1b,
	OUTX_L_G = 0x22,
	OUTX_L_XL = 0x28,
	MAG_OFFX_L = 0x2d,
	MAG_OFFX_H = 0x2e,
	MAG_OFFY_L = 0x2f,
	MAG_OFFY_H = 0x30,
	MAG_OFFZ_L = 0x31,
	MAG_OFFZ_H = 0x32,
	FIFO_STATUS1 = 0x3a,
	FIFO_STATUS2 = 0x3b,
	FIFO_STATUS3 = 0x3c,
	FIFO_STATUS4 = 0x3d,
	FIFO_DATA_OUT_L = 0x3e,
	FIFO_DATA_OUT_H = 0x3f,
	TAP_CFG = 0x58,
	WAKE_UP_THS = 0x5b,
	WAKE_UP_DUR = 0x5c,
	MD1_CFG = 0x5e
} lsm6dsm_register_t;

typedef enum {
	SM_THS = 0x13
} lsm6dsm_embedded_conf_t;

typedef struct {
	vec3f_s x_pri;
	vec3f_s x_post;
	vec3f_s v_pri;
	vec3f_s v_post;
} imu_ab_filter_s;

typedef struct {
	nrfx_spim_t *spim;
	int cs_pin;
	quat_s rotation;
	vec3f_s accels;
	imu_ab_filter_s alphaBeta;
	int32_t gyro_x_offset;
	int32_t gyro_y_offset;
	int32_t gyro_z_offset;
} imu_handle_s;

typedef struct {
	vec3f_s position;
	uint16_t batt_voltage;
	uint16_t cpu_usage;
	uint8_t bytes_count;
	uint8_t imus_count;
} misc_data_s;

static vec3f_s dev_velocity;
static vec3f_s dev_position;
static nrfx_spim_t spi1 = NRFX_SPIM_INSTANCE(0);
static nrfx_spim_t spi2 = NRFX_SPIM_INSTANCE(1);

const float DELTA_T = (float)(1.0f/(float)IMU_SAMPLE_F);		// sampling period in seconds

#define NRFX_SPIM1_GEN_CONFIG                             		\
{                                                            	\
    .sck_pin        = 9,                						\
    .mosi_pin       = 8,            			    			\
    .miso_pin       = 7,			                			\
    .ss_pin         = NRFX_SPIM_PIN_NOT_USED,                	\
    .ss_active_high = false,                                 	\
    .irq_priority   = NRFX_SPIM_DEFAULT_CONFIG_IRQ_PRIORITY, 	\
    .orc            = 0xFF,                                  	\
    .frequency      = NRF_SPIM_FREQ_8M,                      	\
    .mode           = NRF_SPIM_MODE_0,                       	\
    .bit_order      = NRF_SPIM_BIT_ORDER_MSB_FIRST,          	\
}

#define NRFX_SPIM2_GEN_CONFIG                             		\
{                                                            	\
    .sck_pin        = 4,                						\
    .mosi_pin       = 3,            			    			\
    .miso_pin       = 2,			                			\
    .ss_pin         = NRFX_SPIM_PIN_NOT_USED,                	\
    .ss_active_high = false,                                 	\
    .irq_priority   = NRFX_SPIM_DEFAULT_CONFIG_IRQ_PRIORITY, 	\
    .orc            = 0xFF,                                  	\
    .frequency      = NRF_SPIM_FREQ_8M,                      	\
    .mode           = NRF_SPIM_MODE_0,                       	\
    .bit_order      = NRF_SPIM_BIT_ORDER_MSB_FIRST,          	\
}

inline void PORT_SetDevState(dev_state_t state) {
	dev_state = state;
}

void PORT_SpiInit() {
	nrfx_spim_config_t spi1_config = NRFX_SPIM1_GEN_CONFIG;
	nrfx_spim_config_t spi2_config = NRFX_SPIM2_GEN_CONFIG;
	APP_ERROR_CHECK(nrfx_spim_init(&spi1, &spi1_config, NULL, NULL));
	APP_ERROR_CHECK(nrfx_spim_init(&spi2, &spi2_config, NULL, NULL));
}

void PORT_SpiTx(uint8_t* buf, int length, imu_handle_s *imu) {
	nrf_gpio_pin_clear(imu->cs_pin);
	imu->spim->p_reg->TXD.PTR = (uint32_t)buf;
	imu->spim->p_reg->TXD.MAXCNT = length;
	imu->spim->p_reg->RXD.MAXCNT = 0;
	imu->spim->p_reg->EVENTS_END = 0x0UL;
	imu->spim->p_reg->TASKS_START = 0x1UL;
	while(imu->spim->p_reg->EVENTS_END == 0x0UL);
	nrf_gpio_pin_set(imu->cs_pin);
}

void PORT_SpiTxRx(uint8_t* tx_buf, int tx_length, uint8_t* rx_buf, int rx_length, imu_handle_s *imu) {
	nrf_gpio_pin_clear(imu->cs_pin);
	/* tx data */
	imu->spim->p_reg->TXD.PTR = (uint32_t)tx_buf;
	imu->spim->p_reg->TXD.MAXCNT = tx_length;
	imu->spim->p_reg->RXD.MAXCNT = 0;
	imu->spim->p_reg->EVENTS_END = 0x0UL;
	imu->spim->p_reg->TASKS_START = 0x1UL;
	while(imu->spim->p_reg->EVENTS_END == 0x0UL);
	/* rx data */
	imu->spim->p_reg->RXD.PTR = (uint32_t)rx_buf;
	imu->spim->p_reg->RXD.MAXCNT = rx_length;
	imu->spim->p_reg->TXD.MAXCNT = 0;
	imu->spim->p_reg->EVENTS_END = 0x0UL;
	imu->spim->p_reg->TASKS_START = 0x1UL;
	while(imu->spim->p_reg->EVENTS_END == 0x0UL);
	nrf_gpio_pin_set(imu->cs_pin);
}

static void ImuWriteRegister(lsm6dsm_register_t addr, uint8_t val, imu_handle_s *imu) {
	uint8_t data[] = { addr & 0b0111111, val };					// adding 'write' bit
	PORT_SpiTx(data, 2, imu);
}

static void ImuReadRegister(lsm6dsm_register_t addr, uint8_t *val, uint16_t count, imu_handle_s *imu) {
	volatile uint8_t m_addr = addr | 0b10000000;				// adding 'read' bit
	PORT_SpiTxRx((uint8_t *)&m_addr, 1, val, count, imu);
}

static void ImuReset(imu_handle_s *imu) {
	volatile uint8_t data = 1;
	ImuWriteRegister(CTRL3_C, data, imu);
	nrf_delay_ms(20);
	ImuReadRegister(CTRL3_C, (uint8_t *)&data, 1, imu);
	IASSERT(!(data & 1));				// if device holds a reset state
	ImuReadRegister(WHO_AM_I, (uint8_t *)&data, 1, imu);
	IASSERT(data == 0x6a);
}

static inline void AccFilter(imu_handle_s *imu) {
//	static float gxFilt = 0.0;
//	static float gyFilt = 0.0;
//	static float filtAlpha = 0.3;
//	gxFilt = (1 - filtAlpha)*gxFilt + filtAlpha*imu->acc_x;
//	imu->acc_x = gxFilt;
//	gyFilt = (1 - filtAlpha)*gyFilt + filtAlpha*imu->acc_y;
//	imu->acc_y = gyFilt;
}

static void MadgwickFilterUpdate(float w_x_t, float w_y_t, float w_z_t, float a_x_t, float a_y_t, float a_z_t, imu_handle_s *imu) {
	const float GYRO_ERROR = 3.14159265358979f * (1.0f / 180.0f);			// gyroscope measurement error in rad/s (shown as 1 deg/s)
	const float BETA = sqrt(3.0f / 4.0f) * GYRO_ERROR;						// compute beta
	/* Local system variables */
	float norm; 															// vector norm
	float SEqDot_omega_1, SEqDot_omega_2, SEqDot_omega_3, SEqDot_omega_4; 	// quaternion derrivative from gyroscopes elements
	float f_1, f_2, f_3; 													// objective function elements
	float J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33; 				// objective function Jacobian elements
	float SEqHatDot_1, SEqHatDot_2, SEqHatDot_3, SEqHatDot_4; 				// estimated direction of the gyroscope error

	/* Axulirary variables to avoid repeated calcualtions */
	float halfSEq_1 = 0.5f * imu->rotation.W;
	float halfSEq_2 = 0.5f * imu->rotation.Q1;
	float halfSEq_3 = 0.5f * imu->rotation.Q2;
	float halfSEq_4 = 0.5f * imu->rotation.Q3;
	float twoSEq_1 = 2.0f * imu->rotation.W;
	float twoSEq_2 = 2.0f * imu->rotation.Q1;
	float twoSEq_3 = 2.0f * imu->rotation.Q2;

	/* Normalise the accelerometer measurement */
	norm = sqrt(a_x_t * a_x_t + a_y_t * a_y_t + a_z_t * a_z_t);
	a_x_t /= norm;
	a_y_t /= norm;
	a_z_t /= norm;

	/* Compute the objective function and Jacobian */
	f_1 = twoSEq_2 * imu->rotation.Q3 - twoSEq_1 * imu->rotation.Q2 - a_x_t;
	f_2 = twoSEq_1 * imu->rotation.Q1 + twoSEq_3 * imu->rotation.Q3 - a_y_t;
	f_3 = 1.0f - twoSEq_2 * imu->rotation.Q1 - twoSEq_3 * imu->rotation.Q2 - a_z_t;
	J_11or24 = twoSEq_3; 													// J_11 negated in matrix multiplication
	J_12or23 = 2.0f * imu->rotation.Q3;
	J_13or22 = twoSEq_1; 													// J_12 negated in matrix multiplication
	J_14or21 = twoSEq_2;
	J_32 = 2.0f * J_14or21; 												// negated in matrix multiplication
	J_33 = 2.0f * J_11or24; 												// negated in matrix multiplication

	/* Compute the gradient (matrix multiplication) */
	SEqHatDot_1 = J_14or21 * f_2 - J_11or24 * f_1;
	SEqHatDot_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3;
	SEqHatDot_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1;
	SEqHatDot_4 = J_14or21 * f_1 + J_11or24 * f_2;

	/* Normalise the gradient */
	norm = sqrt(SEqHatDot_1 * SEqHatDot_1 + SEqHatDot_2 * SEqHatDot_2 + SEqHatDot_3 * SEqHatDot_3 + SEqHatDot_4 * SEqHatDot_4);
	SEqHatDot_1 /= norm;
	SEqHatDot_2 /= norm;
	SEqHatDot_3 /= norm;
	SEqHatDot_4 /= norm;

	/* Compute the quaternion derrivative measured by gyroscopes */
	SEqDot_omega_1 = -halfSEq_2 * w_x_t - halfSEq_3 * w_y_t - halfSEq_4 * w_z_t;
	SEqDot_omega_2 = halfSEq_1 * w_x_t + halfSEq_3 * w_z_t - halfSEq_4 * w_y_t;
	SEqDot_omega_3 = halfSEq_1 * w_y_t - halfSEq_2 * w_z_t + halfSEq_4 * w_x_t;
	SEqDot_omega_4 = halfSEq_1 * w_z_t + halfSEq_2 * w_y_t - halfSEq_3 * w_x_t;

	/* Compute then integrate the estimated quaternion derrivative */
	imu->rotation.W += (SEqDot_omega_1 - (BETA * SEqHatDot_1)) * DELTA_T;
	imu->rotation.Q1 += (SEqDot_omega_2 - (BETA * SEqHatDot_2)) * DELTA_T;
	imu->rotation.Q2 += (SEqDot_omega_3 - (BETA * SEqHatDot_3)) * DELTA_T;
	imu->rotation.Q3 += (SEqDot_omega_4 - (BETA * SEqHatDot_4)) * DELTA_T;

	/* Normalise quaternion */
	norm = sqrt(imu->rotation.W * imu->rotation.W + imu->rotation.Q1 * imu->rotation.Q1 + imu->rotation.Q2 * imu->rotation.Q2 + imu->rotation.Q3 * imu->rotation.Q3);
	imu->rotation.W /= norm;
	imu->rotation.Q1 /= norm;
	imu->rotation.Q2 /= norm;
	imu->rotation.Q3 /= norm;
}

static void AlphaBetaFilterUpdate(float *w_x_t, float *w_y_t, float *w_z_t, imu_handle_s *imu) {
	const float alpha = 0.7f;
	const float beta = 0.05f;
	imu_ab_filter_s *ab = &imu->alphaBeta;

	ab->x_pri.x = ab->x_post.x + DELTA_T * ab->v_post.x;
	ab->x_pri.y = ab->x_post.y + DELTA_T * ab->v_post.y;
	ab->x_pri.z = ab->x_post.z + DELTA_T * ab->v_post.z;

	ab->v_pri.x = ab->v_post.x;
	ab->v_pri.y = ab->v_post.y;
	ab->v_pri.z = ab->v_post.z;

	ab->x_post.x = ab->x_pri.x + alpha * (*w_x_t - ab->x_pri.x);
	ab->x_post.y = ab->x_pri.y + alpha * (*w_y_t - ab->x_pri.y);
	ab->x_post.z = ab->x_pri.z + alpha * (*w_z_t - ab->x_pri.z);

	ab->v_post.x = ab->v_pri.x + beta * (*w_x_t - ab->x_pri.x) / DELTA_T;
	ab->v_post.y = ab->v_pri.y + beta * (*w_y_t - ab->x_pri.y) / DELTA_T;
	ab->v_post.z = ab->v_pri.z + beta * (*w_z_t - ab->x_pri.z) / DELTA_T;

	*w_x_t = ab->x_post.x;
	*w_y_t = ab->x_post.y;
	*w_z_t = ab->x_post.z;
}

#if CALCULATE_POS
static inline void calculatePostion(imu_handle_s *imu) {
	const int Xmax = 8400; const int Xmin = 8400;
	const int Ymax = 8200; const int Ymin = 8370;
	const int Zmax = 8390; const int Zmin = 8000;
	/* Rotate the G vector inversely to remove the gravitation from accel values */
	vec3f_s v = {
		.x = 0,
		.y = 0,
		.z = 1
	};
	vec3f_s res = rotateInverse(v, imu->rotation);
	imu->accels.x -= (res.x > 0) ? res.x*Xmax : res.x*Xmin;
	imu->accels.y -= (res.y > 0) ? res.y*Ymax : res.y*Ymin;
	imu->accels.z -= (res.z > 0) ? res.z*Zmax : res.z*Zmin;
	/* Rotate the accel values to get a vector that is independent from the rotation */
	v.x = imu->accels.x;
	v.y = imu->accels.y;
	v.z = imu->accels.z;
	res = rotate(v, imu->rotation);
	/* Scale the measured accelerations to milimeters/s^2 */
	imu->accels.x = res.x*1.22f;
	imu->accels.y = res.y*1.22f;
	imu->accels.z = res.z*1.22f;
	/* Integrate the acceleration twice to get the device's position */
	dev_velocity.x = (fabsf(imu->accels.x) < 35.0f) ? 0.0f : dev_velocity.x + imu->accels.x*DELTA_T;
	dev_velocity.y = (fabsf(imu->accels.y) < 35.0f) ? 0.0f : dev_velocity.y + imu->accels.y*DELTA_T;
	dev_velocity.z = (fabsf(imu->accels.z) < 50.0f) ? 0.0f : dev_velocity.z + imu->accels.z*DELTA_T;
	dev_position.x += dev_velocity.x*DELTA_T;
	dev_position.y += dev_velocity.y*DELTA_T;
	dev_position.z += dev_velocity.z*DELTA_T;
}
#endif

static void ImuGetAccValues(int16_t *x, int16_t *y, int16_t *z, imu_handle_s *imu) {
	volatile uint8_t data[6] = { 0 };
	ImuReadRegister(OUTX_L_XL, (uint8_t *)data, 6, imu);
	*x = (int16_t)((data[1] << 8) | data[0]);
	*y = (int16_t)((data[3] << 8) | data[2]);
	*z = (int16_t)((data[5] << 8) | data[4]);
}

static void ImuSetQuaternionFromAccel(imu_handle_s *imu) {
	int16_t x, y, z;
	ImuGetAccValues(&x, &y, &z, imu);
	float roll = atan2f(y , z);
	float pitch = atan2f((-x) , sqrtf(y*y + z*z));
	float yaw = 0.0f;

	float cy = cosf(yaw * 0.5);
	float sy = sinf(yaw * 0.5);
	float cr = cosf(roll * 0.5);
	float sr = sinf(roll * 0.5);
	float cp = cosf(pitch * 0.5);
	float sp = sinf(pitch * 0.5);

	imu->rotation.W = cy * cr * cp + sy * sr * sp;	// W
	imu->rotation.Q1 = cy * sr * cp - sy * cr * sp;	// X
	imu->rotation.Q2 = cy * cr * sp + sy * sr * cp;	// Y
	imu->rotation.Q3 = sy * cr * cp - cy * sr * sp;	// Z
}

static int ImuGetFifoCount(imu_handle_s *imu) {
	volatile uint8_t fifo_stat[2];
	ImuReadRegister(FIFO_STATUS1, (uint8_t *)fifo_stat, 2, imu);
	return (int)((((uint16_t)fifo_stat[1] & 0b111) << 8) | fifo_stat[0]);
}

static inline int ImuFilterData(imu_handle_s *imu, bool calculatePosition) {
	const float DEG_TO_RAD = (float)(3.14159265358979/180.0);
	enum { DATA_BUF_SIZE = 4096 };
	int fifo_count = ImuGetFifoCount(imu);
	fifo_count -= fifo_count % 12;
	IASSERT(fifo_count < DATA_BUF_SIZE);
	if(0 < fifo_count) {
		volatile uint8_t data[DATA_BUF_SIZE];
		ImuReadRegister(FIFO_DATA_OUT_L, (uint8_t *)data, fifo_count, imu);
		for(uint16_t i = 0; i < fifo_count; i += 12) {
			float w_x = (int16_t)(((data[i + 1] << 8) | data[i]) + imu->gyro_x_offset)*0.07f*DEG_TO_RAD;
			float w_y = (int16_t)(((data[i + 3] << 8) | data[i + 2]) + imu->gyro_y_offset)*0.07f*DEG_TO_RAD;
			float w_z = (int16_t)(((data[i + 5] << 8) | data[i + 4]) + imu->gyro_z_offset)*0.07f*DEG_TO_RAD;
			imu->accels.x = (int16_t)(((data[i + 7] << 8) | data[i + 6]));
			imu->accels.y = (int16_t)(((data[i + 9] << 8) | data[i + 8]));
			imu->accels.z = (int16_t)(((data[i + 11] << 8) | data[i + 10]));
			AlphaBetaFilterUpdate(&w_x, &w_y, &w_z, imu);
			MadgwickFilterUpdate(w_x, w_y, w_z, imu->accels.x, imu->accels.y, imu->accels.z, imu);
#if CALCULATE_POS
			if (calculatePosition) {
				calculatePostion(imu);
			}
#endif
		}
		return 0;
	} else {
		return -1;
	}
}

static void ImusSendMeasData(imu_handle_s imus_array[], const int imus_count) {
	uint8_t data[512];
	uint16_t offset = 0;
	uint16_t bytes_count = sizeof(misc_data_s) + imus_count*sizeof(quat_s);
	misc_data_s misc_data = {
			.position = dev_position,
			.batt_voltage = (uint16_t)saadc_get_current_batt_voltage(),
			.cpu_usage = (uint16_t)(debug_timer_get()/(REFRESH_T)),
			.bytes_count = bytes_count,
			.imus_count = imus_count
	};
	memcpy(data + offset, &misc_data, sizeof(misc_data_s));
	offset += sizeof(misc_data_s);
	for (int i = 0; i < imus_count; i++) {
		memcpy(data + offset, &imus_array[i].rotation, sizeof(quat_s));
		offset += sizeof(quat_s);
	}
#if !DEBUG_MODE
	BleSendBytes(data, &bytes_count);
#endif
}

static void ImuGpioConfig(imu_handle_s *imu) {
	nrf_gpio_pin_set(imu->cs_pin);
	nrf_gpio_cfg_output(imu->cs_pin);
}

static void ImuGetGyroValues(int16_t *x, int16_t *y, int16_t *z, imu_handle_s *imu) {
	volatile uint8_t data[6] = { 0 };
	ImuReadRegister(OUTX_L_G, (uint8_t *)data, 6, imu);
	*x = (int16_t)((data[1] << 8) | data[0]);
	*y = (int16_t)((data[3] << 8) | data[2]);
	*z = (int16_t)((data[5] << 8) | data[4]);
}

static void ImusSetGyrosOffsets(imu_handle_s imus_array[], const int imus_count) {
	enum { GYRO_OFFSET_REPEATS = 32 };
	for(int i = 0; i < imus_count; i++) {
		imus_array[i].gyro_x_offset = 0;
		imus_array[i].gyro_y_offset = 0;
		imus_array[i].gyro_z_offset = 0;
	}
	for(int i = 0; i < GYRO_OFFSET_REPEATS; i++) {
		for(int i = 0; i < imus_count; i++) {
			int16_t x, y, z;
			ImuGetGyroValues(&x, &y, &z, &imus_array[i]);
			imus_array[i].gyro_x_offset += x;
			imus_array[i].gyro_y_offset += y;
			imus_array[i].gyro_z_offset += z;
		}
		nrf_delay_ms(30);
	}
	for(int i = 0; i < imus_count; i++) {
		imus_array[i].gyro_x_offset /= -GYRO_OFFSET_REPEATS;
		imus_array[i].gyro_y_offset /= -GYRO_OFFSET_REPEATS;
		imus_array[i].gyro_z_offset /= -GYRO_OFFSET_REPEATS;
	}
}

static void ImuContinousMeasStart(imu_handle_s *imu) {
	ImuReset(imu);
	/* Set IMU to continuous data acquisition */
	ImuWriteRegister(CTRL3_C, 0b01000100, imu);
#if CALCULATE_POS
	ImuWriteRegister(CTRL1_XL, 0b00001000 | (0b1000 << 4), imu);
#else
	ImuWriteRegister(CTRL1_XL, 0b00001000 | (IMU_ODR << 4), imu);
#endif
	ImuWriteRegister(CTRL2_G, 0b00 | (IMU_ODR << 4) | (0b11 << 2), imu);
}

static void ImuFifoMeasStart(imu_handle_s *imu) {
#if CALCULATE_POS
	const int LPF2_XL_EN 		= 0b1;
	const int HPCF_XL 			= 0b11;
	const int INPUT_COMPOSITE	= 0b1;
	const int HP_SLOPE_XL_EN	= 0b0;
	/* Configure the accelerometer's filtering */
	ImuWriteRegister(CTRL8_XL, 	(LPF2_XL_EN << 7) |
								(HPCF_XL << 5) |
								(INPUT_COMPOSITE << 3) |
								(HP_SLOPE_XL_EN << 2), imu);
#endif
	/* Set IMU to FIFO mode */
	ImuWriteRegister(FIFO_CTRL1, 0b00000000, imu);
	ImuWriteRegister(FIFO_CTRL2, 0b00000000, imu);
	ImuWriteRegister(FIFO_CTRL3, 0b00001001, imu);
	ImuWriteRegister(FIFO_CTRL4, 0b00000000, imu);
	ImuWriteRegister(FIFO_CTRL5, 0b00000110 | (IMU_ODR << 3), imu);
}

static void ImusCalibrate(imu_handle_s imus_array[], const int imus_count, bool resetGyrosOffsets) {
	/* Config IMUs into continous meas mode for calibrating purpose */
	for(int i = 0; i < imus_count; i++)
		ImuContinousMeasStart(&imus_array[i]);
	/* Show that imu is going to calibrate, also wait for gyroscopes to stabilise  */
	set_led_state(LED_GRN, LED_STATE_SET);
	set_led_state(LED_RED, LED_STATE_SET);
	nrf_delay_ms(400);
	set_led_state(LED_GRN, LED_STATE_CLEAR);
	set_led_state(LED_RED, LED_STATE_CLEAR);
	nrf_delay_ms(400);
	/* Indicate calibrating */
	set_led_state(LED_RED, LED_STATE_SET);
	/* Set gyroscopes offsets */
	if (resetGyrosOffsets)
		ImusSetGyrosOffsets(imus_array, imus_count);
	/* Set initial quaternions values */
	for(int i = 0; i < imus_count; i++)
		ImuSetQuaternionFromAccel(&imus_array[i]);
	/* Reset the position and velocity values */
	memset(&dev_velocity, 0, sizeof(vec3f_s));
	memset(&dev_position, 0, sizeof(vec3f_s));
	/* Calibrating finished */
	set_led_state(LED_RED, LED_STATE_CLEAR);
	/* Config IMUs into FIFO meas mode */
	for(int i = 0; i < imus_count; i++)
		ImuFifoMeasStart(&imus_array[i]);
}

static void ImusInit(imu_handle_s imus_array[], const int imus_count) {
	/* Config IMUs CS pins as outputs */
	for(int i = 0; i < imus_count; i++)
		ImuGpioConfig(&imus_array[i]);
}

static inline void ImusFilterAndSend(imu_handle_s imus_array[], const int imus_count) {
	debug_timer_clear();
	ImuFilterData(&imus_array[0], true);
	for(int i = 1; i < imus_count; i++)
		ImuFilterData(&imus_array[i], false);
	ImusSendMeasData(imus_array, imus_count);
	debug_timer_capture();
}

static inline void MeasBattery() {
	set_led_state(LED_RED, LED_STATE_SET);
	saadc_meas_batt_voltage();
	set_led_state(LED_RED, LED_STATE_CLEAR);
}

static inline void IdleBlink() {
	set_led_state(LED_GRN, LED_STATE_SET);
	nrf_delay_ms(1);
	set_led_state(LED_GRN, LED_STATE_CLEAR);
}

static void HandleStates() {
	const int IMUS_MEAS_T = REFRESH_T;
	const int BATT_MEAS_T = 60000;
	const int IDLE_LED_T = 5000;
	static uint32_t imuMeasTick = IMUS_MEAS_T;
	static uint32_t battMeasTick = BATT_MEAS_T;
	static uint32_t idleLedTick = IDLE_LED_T;
	/* Handle different states of the program */
	uint32_t currentTick = SYSTIM_GetTick();
	if ((int)(currentTick - imuMeasTick) >= IMUS_MEAS_T) {
		imuMeasTick = currentTick;
		PORT_SetDevState(MEASURE);
	}
	else if ((int)(currentTick - battMeasTick) >= BATT_MEAS_T) {
		battMeasTick = currentTick;
		MeasBattery();
	}
	else if ((int)(currentTick - idleLedTick) >= IDLE_LED_T) {
		idleLedTick = currentTick;
		IdleBlink();
	}
}

static inline void ClearFpuInterrupts() {
	/* turning off pending FPU interrupts */
	__set_FPSCR(__get_FPSCR()  & ~(0x0000009F));
	(void) __get_FPSCR();
	NVIC_ClearPendingIRQ(FPU_IRQn);
}

static inline void GoToSleep() {
	ClearFpuInterrupts();
	uint32_t currentTick = SYSTIM_GetTick();
	while(currentTick == SYSTIM_GetTick()) {
		sd_app_evt_wait();
	}
}

void PORT_ImuMotionTrackingLoop(bool *if_continue) {
	imu_handle_s imus_array[] = {
			{ &spi1, CS13 },
			{ &spi1, CS1 },
			{ &spi1, CS2 },
			{ &spi1, CS3 },
			{ &spi1, CS4 },
			{ &spi1, CS5 },
			{ &spi1, CS6 },
			{ &spi2, CS7 },
			{ &spi2, CS8 },
			{ &spi2, CS9 },
			{ &spi2, CS10 }
	};
	const int imus_count = sizeof(imus_array)/sizeof(imus_array[0]);
	ImusInit(imus_array, imus_count);
	PORT_SetDevState(STARTUP);
	while(*if_continue) {
		switch (dev_state) {
			case STARTUP:
				ImusCalibrate(imus_array, imus_count, true);
				SYSTIM_ResetTick();
				PORT_SetDevState(IDLE);
				break;

			case CALIBRATE:
				ImusCalibrate(imus_array, imus_count, false);
				PORT_SetDevState(IDLE);
				break;

			case MEASURE:
				ImusFilterAndSend(imus_array, imus_count);
				PORT_SetDevState(IDLE);
				break;

			case IDLE:
				HandleStates();
				GoToSleep();
				break;
		}
	}
}
