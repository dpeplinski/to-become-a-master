/*
 * saadc.c
 *
 *  Created on: Jan 18, 2020
 *      Author: davca
 */
#include "saadc.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrfx_saadc.h"

#define BATT_MEAS_TRIG_PIN	27

static uint32_t current_voltage;

void saadc_event_handler(nrfx_saadc_evt_t const * p_event) {}

void saadc_init() {
	nrf_gpio_pin_set(BATT_MEAS_TRIG_PIN);
	nrf_gpio_cfg_output(BATT_MEAS_TRIG_PIN);
	nrfx_saadc_config_t saadc_config = NRFX_SAADC_DEFAULT_CONFIG;
	nrf_saadc_channel_config_t saadc_channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN4);
	nrfx_saadc_init(&saadc_config, saadc_event_handler);
	nrfx_saadc_channel_init(0, &saadc_channel_config);
	nrf_delay_ms(10);
	saadc_meas_batt_voltage();
}

void saadc_meas_batt_voltage() {
	// Vref+ = 3.6 V (0.6V /(1/6)) = ref_internal/gain
	const double VREF = 3.6;
	nrf_gpio_pin_clear(BATT_MEAS_TRIG_PIN);
	nrf_delay_ms(2);
	nrf_saadc_value_t sum = 0;
	int iters = 4;
	for (int i = 0; i < iters; i++){
		nrf_saadc_value_t voltage;
		nrfx_saadc_sample_convert(0, &voltage);
		sum += voltage;
		nrf_delay_ms(1);
	}
	nrf_gpio_pin_set(BATT_MEAS_TRIG_PIN);
	sum = (int)(sum*VREF*1000/iters/1023);	// voltage on pin (mV)
	current_voltage = sum*2; 				// voltage on battery (after voltage divider)
}

uint32_t saadc_get_current_batt_voltage() {
	return current_voltage;
}
