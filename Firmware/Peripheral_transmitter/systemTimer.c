/*
 * systemTimer.c
 *
 *  Created on: Sep 14, 2019
 *      Author: davca
 */

#include "systemTimer.h"
#include "nrfx_timer.h"

static const nrfx_timer_t SYSTEM_TIMER = NRFX_TIMER_INSTANCE(2);
static volatile uint32_t systemTick;

void systemTimerHandler(nrf_timer_event_t event_type, void * p_context) {
	++systemTick;
}

void SYSTIM_Init() {
	nrfx_timer_config_t system_timer_config = NRFX_TIMER_DEFAULT_CONFIG;
	nrfx_timer_init(&SYSTEM_TIMER, &system_timer_config, systemTimerHandler);
	nrfx_timer_extended_compare(&SYSTEM_TIMER, NRF_TIMER_CC_CHANNEL0, 1000, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
	nrfx_timer_enable(&SYSTEM_TIMER);
}

uint32_t SYSTIM_GetTick() {
	return systemTick;
}

void SYSTIM_ResetTick() {
	systemTick = 0;
}
