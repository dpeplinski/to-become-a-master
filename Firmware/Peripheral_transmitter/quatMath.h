/*
 * quatMath.h
 *
 *  Created on: Aug 12, 2020
 *      Author: davca
 */

#ifndef QUATMATH_H_
#define QUATMATH_H_

/*  q  = W + Q1*i + Q2*j + Q3*k
    q' = W - Q1*i - Q2*j - Q3*k */
typedef struct {
    float W;
    float Q1;
    float Q2;
    float Q3;
} quat_s;

typedef struct {
    float x;
    float y;
    float z;
} vec3f_s;

vec3f_s rotate(vec3f_s v, quat_s q);
vec3f_s rotateInverse(vec3f_s v, quat_s q);

#endif /* QUATMATH_H_ */
