/*
 * quatMath.c
 *
 *  Created on: Aug 12, 2020
 *      Author: davca
 */
#include "math.h"
#include "quatMath.h"

quat_s hamiltonProduct(quat_s q1, quat_s q2) {
    quat_s result = {
        .W = q1.W*q2.W - q1.Q1*q2.Q1 - q1.Q2*q2.Q2 - q1.Q3*q2.Q3,
        .Q1 = q1.W*q2.Q1 + q1.Q1*q2.W + q1.Q2*q2.Q3 - q1.Q3*q2.Q2,
        .Q2 = q1.W*q2.Q2 - q1.Q1*q2.Q3 + q1.Q2*q2.W + q1.Q3*q2.Q1,
        .Q3 = q1.W*q2.Q3 + q1.Q1*q2.Q2 - q1.Q2*q2.Q1 + q1.Q3*q2.W
    };
    return result;
}

vec3f_s rotate(vec3f_s v, quat_s q) {
    /* Rotate a vector with a Quaternion using the Hamilton product
    v' = q*v*q'
    v' = H(H(q,v), q') */
    quat_s qPrim = {
        .W = q.W,
        .Q1 = -q.Q1,
        .Q2 = -q.Q2,
        .Q3 = -q.Q3
    };
    quat_s vQuat = {
        .W = 0.0f,
        .Q1 = v.x,
        .Q2 = v.y,
        .Q3 = v.z
    };
    quat_s vPrim = hamiltonProduct(hamiltonProduct(q, vQuat), qPrim);
    v.x = vPrim.Q1;
    v.y = vPrim.Q2;
    v.z = vPrim.Q3;
    return v;
}

vec3f_s rotateOverVector(vec3f_s v, vec3f_s axis, float angle) {
    quat_s q = {
        .W = cosf(angle/2),
        .Q1 = sinf(angle/2)*axis.x,
        .Q2 = sinf(angle/2)*axis.y,
        .Q3 = sinf(angle/2)*axis.z
    };
    return rotate(v, q);
}

vec3f_s rotateInverse(vec3f_s v, quat_s q) {
    float denom = q.W*q.W + q.Q1*q.Q1 + q.Q2*q.Q2 + q.Q3*q.Q3;
    quat_s qInv = {
        .W = q.W/denom,
        .Q1 = -q.Q1/denom,
        .Q2 = -q.Q2/denom,
        .Q3 = -q.Q3/denom
    };
    return rotate(v, qInv);
}

//static inline void RotateSystem(imu_handle_s *imu) {
//	float roll = atan2f(2.0f*(imu->measures.W*imu->measures.Q1 + imu->measures.Q2*imu->measures.Q3), 1.0f - 2.0f*(imu->measures.Q1*imu->measures.Q1 + imu->measures.Q2*imu->measures.Q2));
//	float pitch = asinf(2.0f*(imu->measures.W*imu->measures.Q2 - imu->measures.Q3*imu->measures.Q1));
//	float yaw = atan2f(2.0f*(imu->measures.Q1*imu->measures.Q2 + imu->measures.Q3*imu->measures.W), 1.0f - 2.0f*(imu->measures.Q2*imu->measures.Q2 + imu->measures.Q3*imu->measures.Q3));
//
//	float cy = cosf(yaw);
//	float sy = sinf(yaw);
//	float cp = cosf(pitch);
//	float sp = sinf(pitch);
//	float cr = cosf(roll);
//	float sr = sinf(roll);
//
//	/* Actual X, Y and Z accelerations with Z as earth gradient */
//	float r_acc_X = imu->measures.acc_x*cp*cy + imu->measures.acc_y*(-cr*sy + sr*sp*cy) + imu->measures.acc_z*(sr*sy + cr*sp*cy);
//	float r_acc_Y = imu->measures.acc_x*cp*sy + imu->measures.acc_y*(cr*cy + sr*sp*sy) + imu->measures.acc_z*(-sr*cy + cr*sp*sy);
//	float r_acc_Z = imu->measures.acc_x*(-sp) + imu->measures.acc_y*sr*cp + imu->measures.acc_z*cr*cp;
//	imu->measures.acc_x = r_acc_X;
//	imu->measures.acc_y = r_acc_Y;
//	imu->measures.acc_z = r_acc_Z;
//}

//////////////////////////////////////////////////////////////
/* DEBUG functions; quaternion from YPR and conversely */
//////////////////////////////////////////////////////////////
//void SetQvals(imu_handle_s *imu) {
//	float roll = 0.0f;	// X
//	float pitch = 0.0f;	// Y
//	float yaw = 0.0f;		// Z
//	float cy = cosf(yaw * 0.5);
//	float sy = sinf(yaw * 0.5);
//	float cr = cosf(roll * 0.5);
//	float sr = sinf(roll * 0.5);
//	float cp = cosf(pitch * 0.5);
//	float sp = sinf(pitch * 0.5);
//
//	imu->measures.W = cy * cr * cp + sy * sr * sp;	// W
//	imu->measures.Q1 = cy * sr * cp - sy * cr * sp;	// X
//	imu->measures.Q2 = cy * cr * sp + sy * sr * cp;	// Y
//	imu->measures.Q3 = sy * cr * cp - cy * sr * sp;	// Z
//
//	roll = atan2f(2.0*(imu->measures.W*imu->measures.Q1 + imu->measures.Q2*imu->measures.Q3), 1 - 2.0*(imu->measures.Q1*imu->measures.Q1 + imu->measures.Q2*imu->measures.Q2));
//	pitch = asinf(2.0*(imu->measures.W*imu->measures.Q2 - imu->measures.Q3*imu->measures.Q1));
//	yaw = atan2f(2.0*(imu->measures.Q1*imu->measures.Q2 + imu->measures.Q3*imu->measures.W), 1 - 2.0*(imu->measures.Q2*imu->measures.Q2 + imu->measures.Q3*imu->measures.Q3));
//}

/* Get actual accelerations from accelerometer and YPR towards the earth;
 * using rotation matrix from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles */
//	float X = -0.672f;		// acc_x
//	float Y = 0.441f;		// acc_y
//	float Z = 0.569f;		// acc_z
//	float yaw = 0.609f;
//	float pitch = 0.745f;
//	float roll = 0.658f;
//
//	float cy = cos(yaw);
//	float sy = sin(yaw);
//	float cp = cos(pitch);
//	float sp = sin(pitch);
//	float cr = cos(roll);
//	float sr = sin(roll);
//
//	float rx = X*cp*cy + Y*(-cr*sy + sr*sp*cy) + Z*(sr*sy + cr*sp*cy);		// actual X, Y and Z accelerations with Z as earth gradient
//	float ry = X*cp*sy + Y*(cr*cy + sr*sp*sy) + Z*(-sr*cy + cr*sp*sy);
//	float rz = X*(-sp) + Y*sr*cp + Z*cr*cp;
//////////////////////////////////////////////////////////////
